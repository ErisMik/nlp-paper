# Use the default python 3.6 image
FROM python:3.6

RUN mkdir -p /usr/src/nlp
WORKDIR /usr/src/nlp

# Install some base requirementa
RUN apt-get -y update && apt-get install -y default-jre wget unzip

# Install the NLP libraries and download their constituents
RUN pip install spacy nltk
RUN python -c "import nltk; nltk.download('punkt'); nltk.download('wordnet'); nltk.download('stopwords'); nltk.download('averaged_perceptron_tagger'); nltk.download('maxent_ne_chunker'); nltk.download('words');"
RUN python -m spacy download en_core_web_lg
RUN python -m spacy download en

# Install the rest of the python requirments
RUN pip install tika plotly scikit-learn regex line_profiler psutil
