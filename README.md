# nlp-paper

## TODO:
- Reduce noise from DOL
    - Intersect with community / instruments list ?
- Reduce noise from Community / Instruments
    - Fix capitalization
    - Deal with disagreeing Enitity Classifications
- Sentiment analysis

## Goal

## Installation / Usage

### Requirements
- docker
- docker-compose


### Running
```
sudo docker-compose run nlp python main.py --process [command ...]
```

## Procedure

## Results

## Resources:
### Tools Used:
- Apache Tika
- spaCy
- nltk

### Research:
- https://nlpforhackers.io/training-ner-large-dataset/
- https://medium.com/district-data-labs/named-entity-recognition-and-classification-for-entity-extraction-6f23342aa7c5
- Text Analytics with Python: A Practical Real-World Approach to Gaining Actionable Insights from Your Data
