import argparse
import logging
from logging.config import dictConfig

from nlp.scrape import scrape_pdf
from nlp.normalize import normalize_corpus
from nlp.compare.compare import compare_chat_plus
from nlp import cache_tools

logging_config = dict(
    version = 1,
    formatters = {
        'f': {'format':
              '%(name)-12s %(levelname)-8s %(message)s'}
        },
    handlers = {
        'h': {'class': 'logging.StreamHandler',
              'formatter': 'f',
              'level': logging.DEBUG}
        },
    root = {
        'handlers': ['h'],
        'level': logging.DEBUG,
        },
    disable_existing_loggers = False,
)

dictConfig(logging_config)
logger = logging.getLogger(__name__)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Eric\'s NLP Tool')
    parser.add_argument('--cache', type=str, nargs='+', help='Supply a command to cache-tools', choices=['stats', 'clean'])
    parser.add_argument('--scrape', action='store_true', help='Use this flag to perform pdf scraping of the dataset')
    parser.add_argument('--process', type=str, nargs='+', help='Supply a type of processing to do on the dataset', choices=['normalize', 'word_freq', 'instruments', 'community', 'dol'])
    parser.add_argument('-p', type=int, default=4, help='Number of multiprocessing threads to use')
    parser.add_argument('-s', action='store_true', help='Skip the preprocess and compare only')
    args = parser.parse_args()

    # Cache-tools case
    if args.cache:
        command = args.cache
        base_cmd = command.pop(0)

        if "clean" == base_cmd:
            filename = command.pop(0)
            cache_tools.clean(filename)

        if "stats" == base_cmd:
            cache_tools.stats()

    # Scrape case
    if args.scrape:
        scrape_pdf("dataset")

    # Process case
    if args.process:
        if "normalize" in args.process:  # Pre-perform the base normalization
            normalize_corpus(processes=args.p)

        task_conf = {
            'word_freq': False,
            'instruments': False,
            'community': False,
            'dol': False,
            'skip_processing': args.s,
        }
        for arg in args.process:
            task_conf[arg] = True

        compare_chat_plus(task_conf, processes=args.p)

