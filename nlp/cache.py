import hashlib
import os

import logging
logger = logging.getLogger(__name__)

from pathlib import Path

def hash_filename(filename):
    hash_object = hashlib.md5(bytes(filename, 'utf-8'))
    return hash_object.hexdigest()

def check_cache(item_name, filename, base="_cache", hashed=False):
    hashed_path = os.path.join(base, hash_filename(item_name), filename)
    if hashed:
        hashed_path = os.path.join(base, item_name, filename)

    cached_file = Path(hashed_path)
    if cached_file.exists():
        return True
    else:
        return False

def write_text_to_cache(content, item_name, filename, base="_cache", hashed=False):
    hashed_path = os.path.join(base, hash_filename(item_name))
    if hashed:
        hashed_path = os.path.join(base, item_name)

    # Create cache dir if DNE
    dir = Path(hashed_path)
    if not dir.exists():
        os.mkdir(hashed_path)

    file_path = os.path.join(hashed_path, filename)
    with open(file_path, 'w') as r_tf:
        for line in content:
            r_tf.write(line)

    logger.debug("Saved %s in %s", filename, hashed_path)