import os
import json

import logging
logger = logging.getLogger(__name__)

def clean(filename, dir="_cache"):
    logger.debug("Running clean in %s : %s", dir, filename)
    for root, dirs, files in os.walk(dir, topdown=False):
        for name in files:
            if filename in name:
                path = os.path.join(root, name)
                os.remove(path)
                logger.debug("Deleted %s", path)

def stats(dir="_cache", printout=True):
    stats = {
        "pdf": {
            "total": 0,
            "CEAA": 0,
            "ASEE": 0,
        },
        "raw_text": {
            "total": 0,
        },
        "normalized_text": {
            "total": 0,
        }
    }

    for root, dirs, files in os.walk(dir, topdown=False):
        for name in files:
            if "meta.json" in name:
                meta_path = os.path.join(root, name)

                with open(meta_path, 'r') as meta_file:
                    metadata = json.loads(meta_file.read())

                stats['pdf']['total'] += 1
                stats['pdf'][metadata['dataset']] += 1

            if "raw_text" in name:
                stats['raw_text']['total'] += 1

            if "normalized_text" in name:
                stats['normalized_text']['total'] += 1

    if printout:
        print("     \t TOTAL \t CEAA \t ASEE")
        print("PDFS:\t {total} \t {CEAA} \t {ASEE}".format(**stats['pdf']))
        print("raw: \t {total}".format(**stats['raw_text']))
        print("norm:\t {total}".format(**stats['normalized_text']))

    return stats
