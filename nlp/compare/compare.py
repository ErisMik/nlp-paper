import os
import json
import spacy

import logging
logger = logging.getLogger(__name__)

import multiprocessing as mp
from pathlib import Path

from nlp.normalize import normalize_text, get_norm_filename
from nlp.cache import check_cache

from nlp.process.word_freq import process_word_freq
from nlp.compare.word_freq import compare_word_freq

from nlp.process.named_entity import process_named_entity, ner_config
from nlp.compare.named_entity import compare_named_entities

from nlp.process.div_of_labour import process_task_phrases
from nlp.compare.div_of_labour import compare_dol

def _preprocess_chat(conf, root, name):
    """
    """
    ## Get the metadata for the document in question
    meta_path = os.path.join(root, name, "meta.json")
    with open(meta_path, 'r') as meta_file:
        metadata = json.loads(meta_file.read())

    ## Process the word freq files
    if conf['word_freq']:
        process_word_freq(root, name)

    conf_ne = conf['community'] or conf['instruments']
    conf_dol = conf['dol']

    ## Process for the CHAT analysis
    ## Prep the NLP pipeline for the document
    if conf_ne or conf_dol:
        normalize_text(root, name, ner_config)

        norm_path = os.path.join(root, name, get_norm_filename(ner_config))
        with open(norm_path, 'r') as norm_file:
            content = norm_file.read()

        nlp = spacy.load('en')
        doc = nlp(content)

    ## Process the named_entities
    if conf_ne:
        process_named_entity(root, name, doc)

    ## Process the division of labour
    if conf_dol:
        process_task_phrases(root, name, doc)

def compare_chat_plus(conf, dir="_cache", num_words=30, processes=4):
    if not conf['skip_processing']:
        mp_pool = mp.Pool(processes=processes)
        mp_results = []

        # Use multiprocessing to do the preprocessing and parsing
        for root, dirs, files in os.walk(dir, topdown=False):
            for name in dirs:
                mp_results.append(mp_pool.apply_async(_preprocess_chat, args=(conf, root, name)))
                # _preprocess_chat(conf, root, name)

        mp_pool.close()
        mp_pool.join()

    ## Compare word_freq:
    if conf['word_freq']:
        compare_word_freq(dir=dir, processes=processes)

    ## Compare the named_entities
    if conf['community'] or conf['instruments']:
        compare_named_entities(dir=dir, processes=processes)

    if conf['dol']:
        compare_dol(dir=dir, processes=processes)

    logger.info("Done !!")
    return True