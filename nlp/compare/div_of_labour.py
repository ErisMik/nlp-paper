import os
import json
import spacy
import pickle
import plotly
import heapq
import plotly.graph_objs as go

import logging
logger = logging.getLogger(__name__)

import multiprocessing as mp
from pathlib import Path
from nltk.corpus import wordnet as wn
from itertools import chain
from operator import itemgetter
from sklearn.manifold import TSNE


TOTAL_CEAA_PAPERS = 1300
TOTAL_ASEE_PAPERS = 23268


KEY_NOUNS = ["student", "assistant", "sessional", "faculty", "faculties", "instructor", "teacher", "professor", "lecturer"]
nlp = None

def _print_freq_plots(sorted_phrases, num_words=30):
    for noun in KEY_NOUNS:
        if noun not in sorted_phrases['CEAA']:
            continue
        if noun not in sorted_phrases['ASEE']:
            continue

        # Get word counts and sort CEAA data
        ceaa_totals = {}
        for item in sorted_phrases['CEAA'][noun]:
            if item in ceaa_totals:
                ceaa_totals[item] += 1
            else:
                ceaa_totals[item] = 1
        ceaa_total_heap = []
        for item, value in ceaa_totals.items():
            heapq.heappush(ceaa_total_heap, (-value, item))
        sorted_ceaa_data = sorted(ceaa_total_heap)[0:num_words]

        # Get word counts and sort ASEE data
        asee_totals = {}
        for item in sorted_phrases['ASEE'][noun]:
            if item in asee_totals:
                asee_totals[item] += 1
            else:
                asee_totals[item] = 1
        asee_total_heap = []
        for item, value in asee_totals.items():
            heapq.heappush(asee_total_heap, (-value, item))
        sorted_asee_data = sorted(asee_total_heap)[0:num_words]

        # Fill in each set with the missing data from the other set
        set_ceaa_data = set(sorted_ceaa_data)
        set_asee_data = set(sorted_asee_data)
        for asee_word in sorted_asee_data:
            for ceaa_word in ceaa_total_heap:
                if asee_word[1] == ceaa_word[1]:
                    set_ceaa_data.add(ceaa_word)
                    break
        for ceaa_word in sorted_ceaa_data:
            for asee_word in asee_total_heap:
                if asee_word[1] == ceaa_word[1]:
                    set_asee_data.add(asee_word)
                    break
        sorted_ceaa_data = sorted(list(set_ceaa_data))
        sorted_asee_data = sorted(list(set_asee_data))

        ## Plot the data
        data = []
        ceaa_trace = {
            'x': [x[1] for x in sorted_ceaa_data],
            'y': [-1* x[0] / TOTAL_CEAA_PAPERS for x in sorted_ceaa_data],
            'name': "CEEA-ACEG",
            'type': 'bar',
        }
        data.append(ceaa_trace)

        asee_trace = {
            'x': [x[1] for x in sorted_asee_data],
            'y': [x[0] / TOTAL_ASEE_PAPERS for x in sorted_asee_data],
            'name': "ASEE",
            'type': 'bar',
        }
        data.append(asee_trace)

        layout = {
          'xaxis': {'title': 'Word'},
          'yaxis': {'title': 'Frequency',},
          'barmode': 'relative',
          'title': 'Word frequency for most frequent tasks assigned to %s role' % noun
        }
        fig = go.Figure({'data': data, 'layout': layout})
        plotly.offline.plot(fig, filename="results/final/dol_%s_frequency"%noun)
        # plotly.io.write_image(fig, filename, format='png')

        # Write to CSV
        with open("results/final/dol_%s_frequency.csv"%noun, 'w') as cvoutfile:
            cvoutfile.write("task,ceea-aceg frequency,asee frequency,\n")
            total_words = set(ceaa_trace['x'] + asee_trace['x'])
            for tword in total_words:
                c_idx = ceaa_trace['x'].index(tword) if tword in ceaa_trace['x'] else None
                a_idx = asee_trace['x'].index(tword) if tword in asee_trace['x'] else None
                c_freq = ceaa_trace['y'][c_idx] if tword in ceaa_trace['x'] else 0
                a_freq = asee_trace['y'][a_idx] if tword in asee_trace['x'] else 0

                cvoutfile.write("%s,%s,%s,\n"%(tword,c_freq, -1*a_freq))

def _print_tsne_plot(filename, data_len, new_values, ceaa_labels, asee_labels, noun):
    ceaa_x = []
    ceaa_y = []
    for value in new_values[:data_len]:
        ceaa_x.append(value[0])
        ceaa_y.append(value[1])
    asee_x = []
    asee_y = []
    for value in new_values[data_len:-1]:
        asee_x.append(value[0])
        asee_y.append(value[1])

    # Create a trace
    chart_data = []

    ceaa_trace = go.Scattergl(
        x = ceaa_x,
        y = ceaa_y,
        name = 'CEAA',
        mode = 'markers',
        marker = dict(
            size = 10,
            color = 'rgba(255, 182, 193, .9)',
            line = dict(
                width = 2,
            )
        ),
        text = ceaa_labels
    )

    asee_trace = go.Scattergl(
        x = asee_x,
        y = asee_y,
        name = 'ASEE',
        mode = 'markers',
        marker = dict(
            size = 10,
            color = 'rgba(0, 182, 193, .9)',
            line = dict(
                width = 2,
            )
        ),
        text = asee_labels
    )

    fig = plotly.tools.make_subplots(rows=1, cols=2)
    fig.append_trace(ceaa_trace, 1, 1)
    fig.append_trace(asee_trace, 1, 2)

    fig['layout'].update(title='t-SNE plot of word vectors for tasks assigned to %s role' % noun)
    plotly.offline.plot(fig, filename=filename)

def _tsne_plot(sorted_phrases, word, filename):
    ceaa_word_list = set(verb for verb in sorted_phrases['CEAA'][word] if not nlp.vocab[verb].is_oov)
    asee_word_list = set(verb for verb in sorted_phrases['ASEE'][word] if not nlp.vocab[verb].is_oov)

    vectors = []
    ceaa_labels = []
    asee_labels = []
    for verb in ceaa_word_list:
        ceaa_labels.append(verb)
        vectors.append(nlp.vocab.get_vector(verb))
    for verb in asee_word_list:
        asee_labels.append(verb)
        vectors.append(nlp.vocab.get_vector(verb))

    cal_perp = int(0.05 * len(vectors))
    for perp in [35, 50, cal_perp]:
        tsne_model = TSNE(perplexity=perp, n_components=2, init='pca', n_iter=7500, learning_rate=200, verbose=1)
        new_values = tsne_model.fit_transform(vectors)
        new_fn = filename + "_%s.html"%perp
        _print_tsne_plot(new_fn, len(ceaa_word_list), new_values, ceaa_labels, asee_labels, word)
        logger.debug("%s done!", perp)

def _visualize_clusters(sorted_phrases):
    # Get the most common words that intersect with CEAA/ASEE
    word_freq_path = Path('results/word_freq_overlapping.txt')
    if not word_freq_path.exists():
        logger.debug("Word freq not done yet, can't cluster")
        return

    freq_words = set()
    with open(word_freq_path, 'r') as infile:
        for line in infile:
            freq_words.update([line.strip(),])

    # Add in some key ones to look at
    freq_words = set()
    freq_words.update(KEY_NOUNS)
    logger.debug(freq_words)

    if not Path('results/dol_tsne').exists():
        os.mkdir('results/dol_tsne')

    for word in freq_words:
        if word in sorted_phrases['CEAA'] and word in sorted_phrases['ASEE']:
            logger.debug("Performing tsne plot for %s", word)
            filename = 'results/final/%s_tsne' % word
            _tsne_plot(sorted_phrases, word, filename)

def _format_results(results, n=30):
    return sorted(results, key=itemgetter(2), reverse=True)[0:n]

def _write_key_results(phrase_comparison, sorted_phrases):
    opposite = {'ASEE':'CEAA', 'CEAA':'ASEE'}
    for dataset in ['ASEE', 'CEAA']:
        for noun in KEY_NOUNS:
            try:
                os.makedirs('results/dol_%s/'%noun, exist_ok=True)

                # Split out the results
                for key, value in phrase_comparison[dataset][noun].items():
                    with open('results/dol_%s/%s_%s_%s.txt'%(noun, noun, dataset, key), 'w') as result_file:
                        for pair in _format_results(value, n=500):
                            result_file.write("{} - {} ({})\n".format(pair[0], pair[1], pair[2]))

                this_set = set(sorted_phrases[dataset][noun])
                opposite_set = set(sorted_phrases[opposite[dataset]][noun])

                # Find the only results
                with open('results/dol_%s/%s_%s_only.txt'%(noun, noun, dataset), 'w') as result_file:
                    for word in this_set.difference(opposite_set):
                        result_file.write("{}\n".format(word))
            except Exception as e:
                logger.error("SOMETHING WENT WRONG: %s", e)

def _write_results(phrase_dict):
    with open('results/dol_asee.txt', 'w') as result_file:
        for noun, result in phrase_dict['ASEE'].items():
            if len(result['compliments']) + len(result['contradicts']) + len(result['intersects']) + len(result['differs']) > 0:
                result_file.write("[{}]\ncompliments: {}\ncontradicts: {}\nintersects: {}\ndiffers: {}\n\n".format(noun,
                                                                                            _format_results(result['compliments']),
                                                                                            _format_results(result['contradicts']),
                                                                                            _format_results(result['intersects']),
                                                                                            _format_results(result['differs'])))
    with open('results/dol_ceaa.txt', 'w') as result_file:
        for noun, result in phrase_dict['CEAA'].items():
            if len(result['compliments']) + len(result['contradicts']) + len(result['intersects']) + len(result['differs']) > 0:
                result_file.write("[{}]\ncompliments: {}\ncontradicts: {}\nintersects: {}\ndiffers: {}\n\n".format(noun,
                                                                                        _format_results(result['compliments']),
                                                                                        _format_results(result['contradicts']),
                                                                                        _format_results(result['intersects']),
                                                                                        _format_results(result['differs'])))
cached_contractions = {}
cached_comparisons = {}

# @profile
def _comp_verb_list(base_verb, base_ss_syns, base_ss_ants, verb_list, syns_name, ants_name):
    noun_data = {syns_name: set(), ants_name: set()}
    for comp_verb in verb_list:
        # Ignore case
        if base_verb == comp_verb:
            continue

        # See if this has already been looked at before
        found = False
        if (comp_verb, base_verb) in cached_comparisons:
            found = True
            cc = cached_comparisons[(comp_verb, base_verb)]
            if cc[0] == "syns":
                noun_data[syns_name].add((comp_verb, base_verb, cc[1]))
            else:
                noun_data[ants_name].add((comp_verb, base_verb, cc[1]))
        elif (base_verb, comp_verb) in cached_comparisons:
            found = True
            cc = cached_comparisons[(base_verb, comp_verb)]
            if cc[0] == "syns":
                noun_data[syns_name].add((base_verb, comp_verb, cc[1]))
            else:
                noun_data[ants_name].add((base_verb, comp_verb, cc[1]))

        # Otherwise find the contradiciton
        if not found:
            if comp_verb in cached_contractions:
                comp_ss_syns, comp_ss_ants = cached_contractions[comp_verb]
            else:
                comp_ss = wn.synsets(comp_verb)
                comp_ss_syns = [ss.lemmas()[0].name() for ss in comp_ss]
                comp_ss_ants = [ss.lemmas()[0].antonyms() for ss in comp_ss]
                comp_ss_ants = [ant.name() for ant in chain.from_iterable(comp_ss_ants)]
                cached_contractions[comp_verb] = (comp_ss_syns, comp_ss_ants)

            vector_sim = nlp.vocab[base_verb].similarity(nlp.vocab[comp_verb])

            # Compliments (Agrees)
            if not set(base_ss_syns).isdisjoint(comp_ss_syns):
                noun_data[syns_name].add((base_verb, comp_verb, vector_sim))
                cached_comparisons[(base_verb, comp_verb)] = ("syns", vector_sim)

            # Contradicts (Disagrees)
            if not set(base_ss_ants).isdisjoint(base_ss_ants):
                noun_data[ants_name].add((base_verb, comp_verb, vector_sim))
                cached_comparisons[(base_verb, comp_verb)] = ("ants", vector_sim)
    return noun_data

# @profile
def _find_syn_and_ant_sets(sorted_phrases, base_ds, comp_ds):
    return_phrase = {}
    for base_noun, verb_list in sorted_phrases[base_ds].items():
        print(base_ds, base_noun)
        noun_data = {'compliments': set(), 'contradicts': set(), 'intersects': set(), 'differs': set()}
        verb_list = set(verb_list)

        for base_verb in verb_list:
            verb_data = {'compliments': set(), 'contradicts': set(), 'intersects': set(), 'differs': set()}

            if base_verb in cached_contractions:
                base_ss_syns, base_ss_ants = cached_contractions[base_verb]
            else:
                base_ss = wn.synsets(base_verb)
                base_ss_syns = [ss.lemmas()[0].name() for ss in base_ss]
                base_ss_ants = [ss.lemmas()[0].antonyms() for ss in base_ss]
                base_ss_ants = [ant.name() for ant in chain.from_iterable(base_ss_ants)]
                cached_contractions[base_verb] = (base_ss_syns, base_ss_ants)

            # Internal comparison
            verb_data.update(_comp_verb_list(base_verb, base_ss_syns, base_ss_ants, verb_list, 'compliments', 'contradicts'))

            # External comparison
            if base_noun in sorted_phrases[comp_ds]:
                verb_list = sorted_phrases[comp_ds][base_noun]
                verb_list = set(verb_list)

                verb_data.update(_comp_verb_list(base_verb, base_ss_syns, base_ss_ants, verb_list, 'intersects', 'differs'))

            # Set 'Update', closer to an add
            noun_data['compliments'].update(verb_data['compliments'])
            noun_data['contradicts'].update(verb_data['contradicts'])
            noun_data['intersects'].update(verb_data['intersects'])
            noun_data['differs'].update(verb_data['differs'])

        return_phrase[base_noun] = noun_data
    return return_phrase

def _extract_task_phrases(root, name):
    """
    Acts as a wrapper to allow for the multiprocessing
    """
    logger.debug('Reading file %s', name)
    meta_path = os.path.join(root, name, "meta.json")
    with open(meta_path, 'r') as meta_file:
        metadata = json.loads(meta_file.read())

    # ner_path = os.path.join(root, name, "tagged_ner")
    # if not Path(ner_path).exists():
    #     logger.debug("TASKPHRASE FILE MISSING")
    #     return None
    # with open(ner_path, 'r') as ner_file:
    #     nerdata = json.loads(ner_file.read())
    nerdata = None

    dol_path = os.path.join(root, name, "tagged_taskphrase")
    if not Path(dol_path).exists():
        logger.debug("TASKPHRASE FILE MISSING")
        return None
    with open(dol_path, 'r') as dol_field:
        doldata = json.loads(dol_field.read())

    return {'meta': metadata, 'phrases': doldata, 'ner': nerdata}

def compare_dol(dir="_cache", num_words=30, processes=4):
    """
    """
    mp_pool = mp.Pool(processes=processes)
    mp_results = []

    for root, dirs, files in os.walk(dir, topdown=False):
        for name in dirs:
            # Add this document to the multiproccessing pool
            mp_results.append(mp_pool.apply_async(_extract_task_phrases, args=(root, name)))
            # _extract_task_phrases(root, name)

    mp_pool.close()
    mp_pool.join()

    # Get the results back from the pool
    task_phrase_data = []
    for result in mp_results:
        if result.get():
            task_phrase_data.append(result.get())

    # Sort out the phrases by dataset
    sorted_phrases = {'ASEE': {}, 'CEAA': {}}
    for result in task_phrase_data:
        dataset = result['meta']['dataset']
        for phrase in result['phrases']:
            if phrase[0].lower() in sorted_phrases[dataset]:
                sorted_phrases[dataset][phrase[0].lower()].append(phrase[1].lower())
            else:
                sorted_phrases[dataset][phrase[0].lower()] = [phrase[1].lower(),]

    # Parse out the results
    phrase_comparison = {'ASEE': {}, 'CEAA': {}}
    global cached_comparisons
    global cached_contractions
    if Path('_cache/_comparisons.pickle').exists():
        with open('_cache/_comparisons.pickle', 'rb') as picklefile:
            cached_comparisons = pickle.load(picklefile)
        logger.debug("Loaded cached comparisons")
    if Path('_cache/_contractions.pickle').exists():
        with open('_cache/_contractions.pickle', 'rb') as picklefile:
            cached_contractions = pickle.load(picklefile)
        logger.debug("Loaded cached contraction syns and ants sets")

    global nlp
    nlp = spacy.load('en_core_web_lg')
    logger.debug("Spacy model loaded!")

    ## Print out the word freq plots
    _print_freq_plots(sorted_phrases)
    return  # Since the other data isn't used for the final just exit here instead

    ## Print t-SNE plot for specific keywords
    _visualize_clusters(sorted_phrases)

    ## Start with the ASEE
    phrase_comparison['ASEE'] = _find_syn_and_ant_sets(sorted_phrases, "ASEE", "CEAA")

    ## End with the CEAA
    phrase_comparison['CEAA'] = _find_syn_and_ant_sets(sorted_phrases, "CEAA", "ASEE")

    with open('_cache/_comparisons.pickle', 'wb') as picklefile:
        pickle.dump(cached_comparisons, picklefile)
    with open('_cache/_contractions.pickle', 'wb') as picklefile:
        pickle.dump(cached_contractions, picklefile)

    _write_results(phrase_comparison)
    _write_key_results(phrase_comparison, sorted_phrases)

    return phrase_comparison
