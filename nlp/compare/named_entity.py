import os
import json
import heapq
import plotly
import plotly.graph_objs as go

import logging
logger = logging.getLogger(__name__)

import multiprocessing as mp
from pathlib import Path

TOTAL_CEAA_PAPERS = 1300
TOTAL_ASEE_PAPERS = 23268

def _print_chart(data, filename, pre_counted=False, num_words=30):
    if not pre_counted:
        totals = {}
        for item in data:
            if item in totals:
                totals[item] += 1
            else:
                totals[item] = 1

        total_heap = []
        for item, value in totals.items():
            heapq.heappush(total_heap, (-value, item))

        sorted_totals = sorted(total_heap)[0:num_words]
    else:
        sorted_totals = data[0:num_words]

    chart_data = [go.Bar(
            x=[x[1]for x in sorted_totals],
            y=[-1 * x[0] for x in sorted_totals]
    )]
    plotly.offline.plot(chart_data, filename=filename)

def _print_combined_chart(ceaa_data, asee_data, filename, title, pre_counted=False, num_words=30):
    if not pre_counted:
        # Get word counts and sort CEAA data
        ceaa_totals = {}
        for item in ceaa_data:
            if item in ceaa_totals:
                ceaa_totals[item] += 1
            else:
                ceaa_totals[item] = 1
        ceaa_total_heap = []
        for item, value in ceaa_totals.items():
            heapq.heappush(ceaa_total_heap, (-value, item))
        sorted_ceaa_data = sorted(ceaa_total_heap)[0:num_words]

        # Get word counts and sort ASEE data
        asee_totals = {}
        for item in asee_data:
            if item in asee_totals:
                asee_totals[item] += 1
            else:
                asee_totals[item] = 1
        asee_total_heap = []
        for item, value in asee_totals.items():
            heapq.heappush(asee_total_heap, (-value, item))
        sorted_asee_data = sorted(asee_total_heap)[0:num_words]

        # Make the data usable for the next bit
        ceaa_data = ceaa_total_heap
        asee_data = asee_total_heap
    else:
        sorted_ceaa_data = ceaa_data[0:num_words]
        sorted_asee_data = asee_data[0:num_words]

    # Fill in each set with the missing data from the other set
    set_ceaa_data = set(sorted_ceaa_data)
    set_asee_data = set(sorted_asee_data)
    for asee_word in sorted_asee_data:
        for ceaa_word in ceaa_data:
            if asee_word[1] == ceaa_word[1]:
                set_ceaa_data.add(ceaa_word)
                break
    for ceaa_word in sorted_ceaa_data:
        for asee_word in asee_data:
            if asee_word[1] == ceaa_word[1]:
                set_asee_data.add(asee_word)
                break
    sorted_ceaa_data = sorted(list(set_ceaa_data))
    sorted_asee_data = sorted(list(set_asee_data))

    ## Plot the data
    data = []
    ceaa_trace = {
        'x': [x[1] for x in sorted_ceaa_data],
        'y': [-1* x[0] / TOTAL_CEAA_PAPERS for x in sorted_ceaa_data],
        'name': "CEEA-ACEG",
        'type': 'bar',
    }
    data.append(ceaa_trace)

    asee_trace = {
        'x': [x[1] for x in sorted_asee_data],
        'y': [x[0] / TOTAL_ASEE_PAPERS for x in sorted_asee_data],
        'name': "ASEE",
        'type': 'bar',
    }
    data.append(asee_trace)

    layout = {
      'xaxis': {'title': 'Word'},
      'yaxis': {'title': 'Instances',},
      'barmode': 'relative',
      'title': title
    }
    fig = go.Figure({'data': data, 'layout': layout})
    plotly.offline.plot(fig, filename=filename)
    # plotly.io.write_image(fig, filename, format='png')

    # Write to CSV
    with open("%s.csv"%filename, 'w') as cvoutfile:
        cvoutfile.write("word,ceea-aceg frequency,asee frequency,\n")
        total_words = set(ceaa_trace['x'] + asee_trace['x'])
        for tword in total_words:
            c_idx = ceaa_trace['x'].index(tword) if tword in ceaa_trace['x'] else None
            a_idx = asee_trace['x'].index(tword) if tword in asee_trace['x'] else None
            c_freq = ceaa_trace['y'][c_idx] if tword in ceaa_trace['x'] else 0
            a_freq = asee_trace['y'][a_idx] if tword in asee_trace['x'] else 0

            cvoutfile.write("%s,%s,%s,\n"%(tword,c_freq, -1*a_freq))

def _find_in_dataset(dataset, ner_data):
    word_count = {}

    not_good_words = []
    total_words = []

    for paper in ner_data:
        paper_dataset = paper['meta']['dataset']
        if paper_dataset == dataset:
            for ner in paper['ner']:
                total_words.append(ner[0])

                if ner[0] in word_count:
                    word_count[ner[0]] += 1
                else:
                    word_count[ner[0]] = 1
        else:
            for ner in paper['ner']:
                not_good_words.append(ner[0])

                if ner[0] in word_count:
                    word_count[ner[0]] += 1
                else:
                    word_count[ner[0]] = 1

    total_words_only = set(total_words).difference(not_good_words)
    total_words_only = [(-1 * word_count[word], word) for word in total_words_only]
    total_words_only.sort()

    return total_words, total_words_only

def _find_in_most_papers(ner_data):
    total_words = {'CEAA': [], 'ASEE': []}
    paper_count = {}
    for paper in ner_data:
        dataset = paper['meta']['dataset']
        for ner in paper['ner']:
            total_words[dataset].append(ner[0])

        words_list = [ner[0] for ner in paper['ner']]
        words_list = set(words_list)
        for word in words_list:
            if word in paper_count:
                paper_count[word] += 1
            else:
                paper_count[word] = 1

    intersection = set(total_words['CEAA']).intersection(set(total_words['ASEE']))

    sorted_intersection = [(-paper_count[word], word) for word in intersection]
    sorted_intersection.sort()

    return sorted_intersection

def _find_most_fequent_intersection(ner_data):
    total_word_count = {'CEAA': {}, 'ASEE': {}}
    total_words = {'CEAA': [], 'ASEE': []}
    for paper in ner_data:
        dataset = paper['meta']['dataset']
        for ner in paper['ner']:
            total_words[dataset].append(ner[0])

            if ner[0] in total_word_count[dataset]:
                total_word_count[dataset][ner[0]] += 1
            else:
                total_word_count[dataset][ner[0]] = 1

    intersection = set(total_words['CEAA']).intersection(set(total_words['ASEE']))
    word_count = {}
    split_word_count = {'ASEE': {}, 'CEAA': {}}
    for word in intersection:
        word_count[word] = total_word_count['ASEE'][word] + total_word_count['CEAA'][word]
        split_word_count['ASEE'][word] = total_word_count['ASEE'][word]
        split_word_count['CEAA'][word] = total_word_count['CEAA'][word]

    sorted_intersection = [(-value, key) for key, value in word_count.items()]
    sorted_intersection.sort()

    split_sorted_intersection = {}
    split_sorted_intersection['ASEE'] = [(-value, key) for key, value in split_word_count['ASEE'].items()]
    split_sorted_intersection['ASEE'].sort()
    split_sorted_intersection['CEAA'] = [(-value, key) for key, value in split_word_count['CEAA'].items()]
    split_sorted_intersection['CEAA'].sort()

    return sorted_intersection, split_sorted_intersection

def _compare_instruments(instrument_data):
    most_frequent_overlap, split_word_count = _find_most_fequent_intersection(instrument_data)
    with open('results/instruments_overlap_most_frequent.txt', 'w') as result_file:
        for word in most_frequent_overlap:
            result_file.write(word[1]+'\n')
    _print_chart(most_frequent_overlap, 'results/instruments_overlap_most_frequent.html', pre_counted=True)

    most_papers_overlap = _find_in_most_papers(instrument_data)
    with open('results/instruments_overlap_most_papers.txt', 'w') as result_file:
        for word in most_papers_overlap:
            result_file.write(word[1]+'\n')
    _print_chart(most_papers_overlap, 'results/instruments_overlap_most_papers.html', pre_counted=True)

    ceaa_instruments, ceaa_instruments_only = _find_in_dataset('CEAA', instrument_data)
    with open('results/instruments_ceaa.txt', 'w') as result_file:
        for word in ceaa_instruments:
            result_file.write(word+'\n')
    with open('results/instruments_ceaa_only.txt', 'w') as result_file:
        for word in ceaa_instruments_only:
            result_file.write(word[1]+'\n')
    _print_chart(ceaa_instruments, 'results/instruments_ceaa.html')
    _print_chart(ceaa_instruments_only, 'results/instruments_ceaa_only.html', pre_counted=True)

    asee_instruments, asee_instruments_only = _find_in_dataset('ASEE', instrument_data)
    with open('results/instruments_asee.txt', 'w') as result_file:
        for word in asee_instruments:
            result_file.write(word+'\n')
    with open('results/instruments_asee_only.txt', 'w') as result_file:
        for word in asee_instruments_only:
            result_file.write(word[1]+'\n')
    _print_chart(asee_instruments, 'results/instruments_asee.html')
    _print_chart(asee_instruments_only, 'results/instruments_asee_only.html', pre_counted=True)

    _print_combined_chart(ceaa_instruments, asee_instruments, 'results/final/instruments', 'Word frequency for most frequent tools for ASEE and CEEA-ACEG')
    _print_combined_chart(split_word_count['CEAA'], split_word_count['ASEE'], 'results/final/instruments_overlap', 'Word frequency for most frequent overlapping tools for ASEE and CEEA-ACEG', pre_counted=True)

def _compare_community(community_data):
    most_frequent_overlap, split_word_count = _find_most_fequent_intersection(community_data)
    with open('results/community_overlap_most_frequent.txt', 'w') as result_file:
        for word in most_frequent_overlap:
            result_file.write(word[1]+'\n')
    _print_chart(most_frequent_overlap, 'results/community_overlap_most_frequent.html', pre_counted=True)

    most_papers_overlap = _find_in_most_papers(community_data)
    with open('results/community_overlap_most_papers.txt', 'w') as result_file:
        for word in most_papers_overlap:
            result_file.write(word[1]+'\n')
    _print_chart(most_papers_overlap, 'results/community_overlap_most_papers.html', pre_counted=True)

    ceaa_community, ceaa_community_only = _find_in_dataset('CEAA', community_data)
    with open('results/community_ceaa.txt', 'w') as result_file:
        for word in ceaa_community:
            result_file.write(word+'\n')
    with open('results/community_ceaa_only.txt', 'w') as result_file:
        for word in ceaa_community_only:
            result_file.write(word[1]+'\n')
    _print_chart(ceaa_community, 'results/community_ceaa.html')
    _print_chart(ceaa_community_only, 'results/community_ceaa_only.html', pre_counted=True)

    asee_community, asee_community_only = _find_in_dataset('ASEE', community_data)
    with open('results/community_asee.txt', 'w') as result_file:
        for word in asee_community:
            result_file.write(word+'\n')
    with open('results/community_asee_only.txt', 'w') as result_file:
        for word in asee_community_only:
            result_file.write(word[1]+'\n')
    _print_chart(asee_community, 'results/community_asee.html')
    _print_chart(asee_community_only, 'results/community_asee_only.html', pre_counted=True)

    _print_combined_chart(ceaa_community, asee_community, 'results/final/community', 'Word frequency use for most frequent community entities for ASEE and CEEA-ACEG')
    _print_combined_chart(split_word_count['CEAA'], split_word_count['ASEE'], 'results/final/community_overlap', 'Word frequency for most frequent overlapping community entities for ASEE and CEEA-ACEG', pre_counted=True)

def _filter(ner_data, filterset):
    filtered_ner_data = []
    for paper in ner_data:
        n_paper = paper.copy()
        filtered_words = []
        for ne in paper['ner']:
            if ne[1] in filterset:
                filtered_words.append(ne)
        n_paper['ner'] = filtered_words
        filtered_ner_data.append(n_paper)
    return filtered_ner_data

def _filter_instruments(ner_data):
    instrument_filters = ['FAC', 'PRODUCT', 'WORK_OF_ART', 'LAW', 'LANGUAGE']
    return _filter(ner_data, instrument_filters)

def _filter_community(ner_data):
    community_filters = ['PERSON', 'NORP', 'ORG', 'GPE']
    return _filter(ner_data, community_filters)

def _extract_named_entities(root, name):
    """
    Extracts the named entities from a text.
    Acts as a wrapper to allow for the multiprocessing
    """
    meta_path = os.path.join(root, name, "meta.json")
    with open(meta_path, 'r') as meta_file:
        metadata = json.loads(meta_file.read())

    ner_path = os.path.join(root, name, "tagged_ner")
    if not Path(ner_path).exists():
        return None
    with open(ner_path, 'r') as tagged_file:
        nerdata = json.loads(tagged_file.read())

    return {'meta': metadata, 'ner': nerdata}

def compare_named_entities(dir="_cache", num_words=30, processes=4):
    """
    Does the named entity comparison and spits it out to a chart
    """
    mp_pool = mp.Pool(processes=processes)
    mp_results = []

    for root, dirs, files in os.walk(dir, topdown=False):
        for name in dirs:
            # Add this document to the multiproccessing pool
            mp_results.append(mp_pool.apply_async(_extract_named_entities, args=(root, name)))

    mp_pool.close()
    mp_pool.join()

    logger.debug("Starting comparing community & instruments")

    # Get the results back from the pool
    ner_data = []
    for result in mp_results:
        if result.get():
            ner_data.append(result.get())

    ## Use the named entities to get and comapre "community"
    instruments_data = _filter_instruments(ner_data)
    _compare_instruments(instruments_data)

    ## Used the named entities to get and compare "community"
    community_data = _filter_community(ner_data)
    _compare_community(community_data)

    return ner_data
