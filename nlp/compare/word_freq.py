import os
import json
import heapq
import plotly
import plotly.graph_objs as go
import multiprocessing as mp

import logging
logger = logging.getLogger(__name__)

from pathlib import Path

from nlp.normalize import normalize_text, get_norm_filename

def _print_combined_chart(ceaa_data, asee_data, filename):
    data = []

    ceaa_trace = {
        'x': [x[1] for x in ceaa_data],
        'y': [-1* x[0] for x in ceaa_data],
        'name': "CEEA-ACEG",
        'type': 'bar',
    }
    data.append(ceaa_trace)

    asee_trace = {
        'x': [x[1] for x in asee_data],
        'y': [x[0] for x in asee_data],
        'name': "ASEE",
        'type': 'bar',
    }
    data.append(asee_trace)

    layout = {
      'xaxis': {'title': 'Word'},
      'yaxis': {'title': 'Instances',},
      'barmode': 'relative',
      'title': 'Instances of use for most frequent words for ASEE and CEEA-ACEG'
    }
    fig = go.Figure({'data': data, 'layout': layout})
    plotly.offline.plot(fig, filename=filename)
    # plotly.io.write_image(fig, filename, format='png')

def _print_combined_freq_chart(ceaa_data, asee_data, totals, filename):
    data = []

    ceaa_trace = {
        'x': [x[1] for x in ceaa_data],
        'y': [-1 * x[0] / totals['CEAA'] for x in ceaa_data],
        'name': "CEEA-ACEG",
        'type': 'bar',
    }
    data.append(ceaa_trace)

    asee_trace = {
        'x': [x[1] for x in asee_data],
        'y': [x[0] / totals['ASEE']  for x in asee_data],
        'name': "ASEE",
        'type': 'bar',
    }
    data.append(asee_trace)

    layout = {
      'xaxis': {'title': 'Word'},
      'yaxis': {'title': 'Frequency',},
      'barmode': 'relative',
      'title': 'Word frequency for most frequent words for ASEE and CEEA-ACEG'
    }
    fig = go.Figure({'data': data, 'layout': layout})
    plotly.offline.plot(fig, filename=filename)
    # plotly.io.write_image(fig, filename, format='png')

    # Write to CSV
    with open("%s.csv"%filename, 'w') as cvoutfile:
        cvoutfile.write("word,ceea-aceg frequency,asee frequency,\n")
        total_words = set(ceaa_trace['x'] + asee_trace['x'])
        for tword in total_words:
            c_idx = ceaa_trace['x'].index(tword) if tword in ceaa_trace['x'] else None
            a_idx = asee_trace['x'].index(tword) if tword in asee_trace['x'] else None
            c_freq = ceaa_trace['y'][c_idx] if tword in ceaa_trace['x'] else 0
            a_freq = asee_trace['y'][a_idx] if tword in asee_trace['x'] else 0

            cvoutfile.write("%s,%s,%s,\n"%(tword,c_freq, -1*a_freq))


def _print_chart(data, filename):
    chart_data = [go.Bar(
            x=[x[1]for x in data],
            y=[-1 * x[0] for x in data]
    )]
    plotly.offline.plot(chart_data, filename=filename)

    txt_filename = filename.split('.')[0] + ".txt"
    with open(txt_filename, 'w') as outfile:
        for word in data:
            outfile.write("%s\n" % word[1])

def _compare_word_freq(root, name):
    normalized_text_path = os.path.join(root, name, get_norm_filename())
    meta_path = os.path.join(root, name, "meta.json")

    # Extract metadata to get dataset
    with open(meta_path, 'r') as meta_file:
        metadata = json.loads(meta_file.read())

    ceaa_words = {}
    asee_words = {}

    logger.debug("Parsing %s", name)
    with open(normalized_text_path, 'r') as norm_file:
            for line in norm_file:
                tokens = line.split(" ")
                for token in tokens:
                    if "ASEE" in metadata["dataset"]:
                        if token in asee_words:
                            asee_words[token] += 1
                        else:
                            asee_words[token] = 1
                    elif "CEAA" in metadata["dataset"]:
                        if token in ceaa_words:
                            ceaa_words[token] += 1
                        else:
                            ceaa_words[token] = 1
                    else:
                        logger.warning("Invalid dataset for %s - %s ", metadata["name"], name)

    return (ceaa_words, asee_words)

def compare_word_freq(dir="_cache", num_words=30, processes=4, chart=True):
    mp_pool = mp.Pool(processes=processes)
    mp_results = []

    ceaa_heap = []
    asee_heap = []
    combined_heap = []
    ceaa_only_heap = []
    asee_only_heap = []

    ceaa_words = {}
    asee_words = {}

    for root, dirs, files in os.walk(dir, topdown=False):
        for name in dirs:
            # Add this document to the multiproccessing pool
            mp_results.append(mp_pool.apply_async(_compare_word_freq, args=(root, name)))
            # _compare_word_freq(root, name)

    mp_pool.close()
    mp_pool.join()

    # Get the results back from the pool
    total_papers = {'total': 0, 'CEAA': 0, 'ASEE': 0}
    for result in mp_results:
        total_papers['total'] += 1
        t_ceaa_words, t_asee_words = result.get()

        if t_ceaa_words:
            total_papers['CEAA'] += 1
        if t_asee_words:
            total_papers['ASEE'] += 1

        for key, value in t_ceaa_words.items():
            if key in ceaa_words:
                ceaa_words[key] += value
            else:
                ceaa_words[key] = value

        for key, value in t_asee_words.items():
            if key in asee_words:
                asee_words[key] += value
            else:
                asee_words[key] = value
    logger.debug("Parsed %s papers", total_papers)

    # Place the results into a heap (Faster than sorting)
    for key, value in ceaa_words.items():
        heapq.heappush(ceaa_heap, (-value, key))

    for key, value in asee_words.items():
        heapq.heappush(asee_heap, (-value, key))

    for key, value in asee_words.items():
        if key in ceaa_words:
            heapq.heappush(combined_heap, (-value - ceaa_words[key], key))
        else:
            heapq.heappush(asee_only_heap, (-value, key))

    for key, value in ceaa_words.items():
        if key not in asee_words:
            heapq.heappush(ceaa_only_heap, (-value, key))

    # Find the largest n number of words and return
    ceaa_largest = []
    asee_largest = []
    combined_largest = []
    ceaa_only_largest = []
    asee_only_largest = []
    for x in range(num_words):
        ceaa_largest.append(heapq.heappop(ceaa_heap))
        ceaa_only_largest.append(heapq.heappop(ceaa_only_heap))

        asee_largest.append(heapq.heappop(asee_heap))
        asee_only_largest.append(heapq.heappop(asee_only_heap))

        combined_largest.append(heapq.heappop(combined_heap))

    # Before returning print to chart if enabled
    if chart:
        _print_chart(ceaa_largest, 'results/word_freq_ceaa.html')
        _print_chart(ceaa_only_largest, 'results/word_freq_ceaa_only.html')
        _print_chart(asee_largest, 'results/word_freq_asee.html')
        _print_chart(asee_only_largest, 'results/word_freq_asee_only.html')
        _print_chart(combined_largest, 'results/word_freq_overlapping.html')

        # Fill in each set with the missing data from the other set
        set_ceaa_data = set(ceaa_largest)
        set_asee_data = set(asee_largest)
        for asee_word in asee_largest:
            for ceaa_word in ceaa_heap:
                if asee_word[1] == ceaa_word[1]:
                    set_ceaa_data.add(ceaa_word)
                    break
        for ceaa_word in ceaa_largest:
            for asee_word in asee_heap:
                if asee_word[1] == ceaa_word[1]:
                    set_asee_data.add(asee_word)
                    break
        ceaa_largest = sorted(list(set_ceaa_data))
        asee_largest = sorted(list(set_asee_data))

        _print_combined_chart(ceaa_largest, asee_largest, 'results/final/word_instances_overlap')
        _print_combined_freq_chart(ceaa_largest, asee_largest, total_papers, 'results/final/word_freq_overlap')

        logger.debug("Charts saved!")

    return {"ceaa": ceaa_largest, "asee": asee_largest}
