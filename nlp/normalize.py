import os
import re
import regex
import unicodedata
import json
import multiprocessing as mp

import nltk

import logging
logger = logging.getLogger(__name__)

from pathlib import Path

from nlp.cache import hash_filename, write_text_to_cache, check_cache

CONTRACTION_MAP = {
    "ain't": "is not",
    "aren't": "are not",
    "can't": "cannot",
    "can't've": "cannot have",
    "'cause": "because",
    "could've": "could have",
    "couldn't": "could not",
    "couldn't've": "could not have",
    "didn't": "did not",
    "doesn't": "does not",
    "don't": "do not",
    "hadn't": "had not",
    "hadn't've": "had not have",
    "hasn't": "has not",
    "haven't": "have not",
    "he'd": "he would",
    "he'd've": "he would have",
    "he'll": "he will",
    "he'll've": "he he will have",
    "he's": "he is",
    "how'd": "how did",
    "how'd'y": "how do you",
    "how'll": "how will",
    "how's": "how is",
    "I'd": "I would",
    "I'd've": "I would have",
    "I'll": "I will",
    "I'll've": "I will have",
    "I'm": "I am",
    "I've": "I have",
    "i'd": "i would",
    "i'd've": "i would have",
    "i'll": "i will",
    "i'll've": "i will have",
    "i'm": "i am",
    "i've": "i have",
    "isn't": "is not",
    "it'd": "it would",
    "it'd've": "it would have",
    "it'll": "it will",
    "it'll've": "it will have",
    "it's": "it is",
    "let's": "let us",
    "ma'am": "madam",
    "mayn't": "may not",
    "might've": "might have",
    "mightn't": "might not",
    "mightn't've": "might not have",
    "must've": "must have",
    "mustn't": "must not",
    "mustn't've": "must not have",
    "needn't": "need not",
    "needn't've": "need not have",
    "o'clock": "of the clock",
    "oughtn't": "ought not",
    "oughtn't've": "ought not have",
    "shan't": "shall not",
    "sha'n't": "shall not",
    "shan't've": "shall not have",
    "she'd": "she would",
    "she'd've": "she would have",
    "she'll": "she will",
    "she'll've": "she will have",
    "she's": "she is",
    "should've": "should have",
    "shouldn't": "should not",
    "shouldn't've": "should not have",
    "so've": "so have",
    "so's": "so as",
    "that'd": "that would",
    "that'd've": "that would have",
    "that's": "that is",
    "there'd": "there would",
    "there'd've": "there would have",
    "there's": "there is",
    "they'd": "they would",
    "they'd've": "they would have",
    "they'll": "they will",
    "they'll've": "they will have",
    "they're": "they are",
    "they've": "they have",
    "to've": "to have",
    "wasn't": "was not",
    "we'd": "we would",
    "we'd've": "we would have",
    "we'll": "we will",
    "we'll've": "we will have",
    "we're": "we are",
    "we've": "we have",
    "weren't": "were not",
    "what'll": "what will",
    "what'll've": "what will have",
    "what're": "what are",
    "what's": "what is",
    "what've": "what have",
    "when's": "when is",
    "when've": "when have",
    "where'd": "where did",
    "where's": "where is",
    "where've": "where have",
    "who'll": "who will",
    "who'll've": "who will have",
    "who's": "who is",
    "who've": "who have",
    "why's": "why is",
    "why've": "why have",
    "will've": "will have",
    "won't": "will not",
    "won't've": "will not have",
    "would've": "would have",
    "wouldn't": "would not",
    "wouldn't've": "would not have",
    "y'all": "you all",
    "y'all'd": "you all would",
    "y'all'd've": "you all would have",
    "y'all're": "you all are",
    "y'all've": "you all have",
    "you'd": "you would",
    "you'd've": "you would have",
    "you'll": "you will",
    "you'll've": "you will have",
    "you're": "you are",
    "you've": "you have"
}

def contains_invalid_text(text):
    if regex.match(r"(Proc\.)|(\©)|(age\s+\d+.)|(CEEA\d+;)|(\d+\s+of\s+\d+)", text):
        return True

    return False

def strip_url(text):
    text = re.sub(r'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)', ' ', text)
    return text

def remove_accented_chars(text):
    text = unicodedata.normalize('NFKD', text).encode('ascii', 'ignore').decode('utf-8', 'ignore')
    return text

def expand_contractions(text, contraction_mapping=CONTRACTION_MAP):

    contractions_pattern = re.compile('({})'.format('|'.join(contraction_mapping.keys())),
                                      flags=re.IGNORECASE|re.DOTALL)
    def expand_match(contraction):
        match = contraction.group(0)
        first_char = match[0]
        expanded_contraction = contraction_mapping.get(match)\
                                if contraction_mapping.get(match)\
                                else contraction_mapping.get(match.lower())
        expanded_contraction = first_char+expanded_contraction[1:]
        return expanded_contraction

    expanded_text = contractions_pattern.sub(expand_match, text)
    expanded_text = re.sub("'", "", expanded_text)
    return expanded_text

def remove_special_characters(text, remove_digits=False):
    pattern = r'[^a-zA-z0-9\s]' if not remove_digits else r'[^a-zA-z\s]'
    text = re.sub(pattern, '', text)
    return text

def remove_extra_whitespace(text):
    text = re.sub(r'[\r|\n|\r\n]+', ' ', text)
    text = re.sub(' +', ' ', text)
    return text

def lemmatize_text(text):
    tokens = nltk.word_tokenize(text)
    wnl = nltk.stem.WordNetLemmatizer()
    text = ' '.join([wnl.lemmatize(word) for word in tokens])
    return text

def remove_stopwords(text):
    tokens = nltk.word_tokenize(text)
    stopword_list = list(nltk.corpus.stopwords.words('english'))
    filtered_tokens = [token for token in tokens if token not in stopword_list]
    filtered_text = ' '.join(filtered_tokens)
    return filtered_text

def remove_short_words(text):
    tokens = nltk.word_tokenize(text)
    filtered_tokens = [token for token in tokens if len(token) > 2]
    filtered_text = ' '.join(filtered_tokens)
    return filtered_text

def _normalize_text(raw_path, name, config, norm_text_name):
    clean_text = ""
    with open(raw_path, 'r') as raw_file:
        for line in raw_file:  # Clean the text in each line
            temp_text = line
            if config['remove_format_lines'] and contains_invalid_text(temp_text):
                continue  # Do not add this line to the loop

            if config['strip_url']: temp_text = strip_url(line)
            if config['remove_accented']: temp_text = remove_accented_chars(line)
            if config['expand_contractions']: temp_text = expand_contractions(temp_text)
            if config['remove_special_characters']: temp_text = remove_special_characters(temp_text, remove_digits=True)
            clean_text += temp_text

        # Normalize the entire text
        if config['lowercase']: clean_text = clean_text.lower()
        if config['remove_extra_whitespace']: clean_text = remove_extra_whitespace(clean_text)
        if config['lemmatize_text']: clean_text = lemmatize_text(clean_text)
        if config['remove_stopwords']: clean_text = remove_stopwords(clean_text)

        if config['remove_short_words']: clean_text = remove_short_words(clean_text)

    write_text_to_cache(clean_text, name, norm_text_name, hashed=True)


DEFAULT_CONFIG = {
    'remove_format_lines': True,
    'strip_url': True,
    'remove_accented': True,
    'expand_contractions': True,
    'remove_special_characters': True,
    'lowercase': True,
    'remove_extra_whitespace': True,
    'lemmatize_text': True,
    'remove_stopwords': True,
    'remove_short_words': True,
}

def normalize_text(root, name, config=DEFAULT_CONFIG):
    logger.debug("Normalizing %s", name)
    raw_path = os.path.join(root, name, "raw_text")
    norm_text_name = get_norm_filename(config)

    exists = Path(raw_path).exists()
    cached = check_cache(name, norm_text_name, hashed=True)
    if not cached and exists:
        logger.debug("normalized_text cache miss: %s with conf %s", name, norm_text_name)
        _normalize_text(raw_path, name, config, norm_text_name)

    elif not exists:
        logger.error("%s is missing raw_text!", name)
        return False

    else:
        logger.debug("normalized_text cache hit: %s with conf %s", name, norm_text_name)

    return True


def normalize_corpus(dir="_cache", config=DEFAULT_CONFIG, processes=4):
    mp_pool = mp.Pool(processes=processes)

    for root, dirs, files in os.walk(dir, topdown=False):
        for name in dirs:
            logger.debug("#### Starting processing ####")
            raw_path = os.path.join(root, name, "raw_text")
            norm_text_name = get_norm_filename(config)

            exists = Path(raw_path).exists()
            cached = check_cache(name, norm_text_name, hashed=True)
            if not cached and exists:
                logger.debug("normalized_text cache miss: %s with conf %s", name, norm_text_name)
                mp_pool.apply_async(_normalize_text, args=(raw_path, name, config, norm_text_name))
                # _normalize_text(raw_path, name, config, norm_text_name)  # Uncomment for debug

            elif not exists:
                logger.error("%s is missing raw_text!", name)

            else:
                logger.debug("normalized_text cache hit: %s with conf %s", name, norm_text_name)

    mp_pool.close()
    mp_pool.join()

def get_norm_filename(config=DEFAULT_CONFIG):
    return "normalized_text_" + hash_filename(json.dumps(config))