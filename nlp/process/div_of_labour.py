import os
import json
import nltk
import spacy
import multiprocessing as mp
from pathlib import Path
from spacy.symbols import nsubj, VERB, PRON, DET

import logging
logger = logging.getLogger(__name__)

from nlp.normalize import normalize_text, get_norm_filename
from nlp.cache import write_text_to_cache, check_cache
from nlp.process.named_entity import ner_config

pos_config = ner_config

def _read_pos(tagged_path, name):
    with open(tagged_path, 'r') as tagged_file:
        content = tagged_file.read()
    return json.loads(content)

def process_task_phrases(root, name, doc):
    normalize_text(root, name, config=pos_config)
    norm_path = os.path.join(root, name, get_norm_filename(pos_config))
    tagged_path = os.path.join(root, name, "tagged_taskphrase")

    exists = Path(norm_path).exists()
    cached = check_cache(name, "tagged_taskphrase", hashed=True)
    if not cached and exists:
        logger.debug("tagged_taskphrase cache miss: %s", name)

        # Find the noun action phrases in the text
        phrases = []
        sents = [sent for sent in doc.sents]
        for x in range(len(sents)):
            for possible_subject in sents[x]:
                if possible_subject.dep == nsubj and possible_subject.head.pos == VERB:
                    # If the possible subject is a proper noun
                    if (not possible_subject.pos == PRON) and (not possible_subject.pos == DET):
                        if not possible_subject.head.is_stop:  # Get rid of some of the noise (is, was, has etc.)
                            phrases.append((possible_subject.lemma_, possible_subject.head.lemma_))
                            continue  # Break from the loop

                    # Otherwise backtrack
                    found = False
                    btx = x
                    while not found and btx > 0:
                        btx -= 1
                        for back_tracked_possible_subject in sents[btx]:
                            if (back_tracked_possible_subject.dep == nsubj) and (back_tracked_possible_subject.head.pos == VERB) and \
                                    (not back_tracked_possible_subject.pos == PRON) and (not back_tracked_possible_subject.pos == DET):
                                if not possible_subject.head.is_stop:  # Get rid of some of the noise (is, was, has etc.)
                                    phrases.append((back_tracked_possible_subject.lemma_, possible_subject.head.lemma_))
                                found = True

        write_text_to_cache(json.dumps(phrases), name, "tagged_taskphrase", hashed=True)
        return phrases

    elif not exists:
        logger.error("%s is missing normalized_text!", name)
        return None

    else:
        logger.debug("tagged_taskphrase cache hit: %s", name)
        return _read_pos(tagged_path, name)
