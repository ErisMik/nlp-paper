import os
import json
import spacy
import nltk
from pathlib import Path

import logging
logger = logging.getLogger(__name__)

from nlp.normalize import normalize_text, get_norm_filename
from nlp.cache import write_text_to_cache, check_cache

ner_config = {
    'remove_format_lines': True,
    'strip_url': True,
    'remove_accented': True,
    'expand_contractions': False,
    'remove_special_characters': False,
    'lowercase': False,
    'remove_extra_whitespace': True,
    'lemmatize_text': False,
    'remove_stopwords': False,
    'remove_short_words': False,
}

def _spacy_ner(doc):
    return [(ent.text, ent.label_) for ent in doc.ents]

def _nltk_ner(doc):
    ents = set()
    for sent in doc.sents:
        text = nltk.pos_tag([token.text for token in sent])
        for entity in nltk.ne_chunk(text):
            if isinstance(entity, nltk.tree.Tree):
                etext = " ".join([word for word, tag in entity.leaves()])
                # label = entity.label()
                ents.add(etext)
    return ents

def _read_ner(tagged_path, name):
    with open(tagged_path, 'r') as tagged_file:
        content = tagged_file.read()
    return json.loads(content)

def process_named_entity(root, name, doc):
    norm_path = os.path.join(root, name, get_norm_filename(ner_config))
    tagged_path = os.path.join(root, name, "tagged_ner")

    exists = Path(norm_path).exists()
    cached = check_cache(name, "tagged_ner", hashed=True)
    if not cached and exists:
        logger.debug("tagged_ner cache miss: %s", name)

        spacy_tagged_content = _spacy_ner(doc)
        nltk_tagged_content = _nltk_ner(doc)
        tagged_content = [entity for entity in spacy_tagged_content if entity[0] in nltk_tagged_content]

        write_text_to_cache(json.dumps(tagged_content), name, "tagged_ner", hashed=True)

        return tagged_content

    elif not exists:
        logger.error("%s is missing normalized_text!", name)
        return None

    else:
        logger.debug("tagged_ner cache hit: %s", name)
        return _read_ner(tagged_path, name)
