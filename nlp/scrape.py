import os
import json
import tika
from tika import parser

import logging
logger = logging.getLogger(__name__)

from .cache import write_text_to_cache, check_cache

def write_metadata(name, pdf_path, dataset):
    """Writes the metadata file to the cache"""
    metadata = {
        'name': name,
        'pdf_path': pdf_path,
        'dataset': dataset
    }
    write_text_to_cache(json.dumps(metadata), name, 'meta.json')

def scrape_pdf(dir="dataset"):
    """Scrapes all the PDFS into raw text.

    Saves the raw text to the cache
    """
    for root, dirs, files in os.walk(dir, topdown=False):
        for name in files:
            if 'pdf' in name:
                logger.debug("#### Starting scrape ####")
                pdf_path = os.path.join(root, name)
                dataset = root.split("/")[1]

                # Write the PDF file metadata
                meta_cached = check_cache(name, "meta.json")
                if not meta_cached:
                    logger.debug("Caching metadata for: %s", name)
                    write_metadata(name, pdf_path, dataset)

                # Extract the raw text from the PDF
                raw_cached = check_cache(name, "raw_text")
                if not raw_cached:
                    logger.debug("raw_text cache miss: %s", pdf_path)
                    parsed = parser.from_file(pdf_path)
                    if "content" in parsed:
                        write_text_to_cache(parsed["content"], name, "raw_text")
                    else:
                        logger.error("Failed to parse %s", pdf_path)
                else:
                    logger.debug("raw_text cache hit: %s", pdf_path)